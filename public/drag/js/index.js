$(function() {
    var drake = dragula([
            document.getElementById('drake-origin'),
            document.getElementById('match-1'),
            document.getElementById('match-2'),
            document.getElementById('match-3'),
            document.getElementById('match-4'),
            document.getElementById('match-5')
        ], {
            accepts: function(el, target, source, sibling) {
                return target.id === "drake-origin" || target.innerHTML === "";
            },
        })
        .on('over', function(el, container, source) {
            //console.log(el, container, source);
        })
        .on('drop', function(el, container, source) {
            provjeri()
        });

});

function shuffle(array) { //izmješaj pitanja
    var i = 0,
        j = 0,
        temp = null

    for (i = array.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1))
        temp = array[i]
        array[i] = array[j]
        array[j] = temp
    }
}

function pitanja() {
    var brojevi = [1, 2, 3, 4, 5]
    shuffle(brojevi)
    brojevi.forEach(function(x) {
        $("#drake-origin").append('<li class="draggable" id="' + x + '"><img src="s' + x + '.png"></li>')
    })
}
pitanja();


function provjeri() {
    if ($("#match-1").html() == "" || $("#match-2").html() == "" || $("#match-3").html() == "" || $("#match-4").html() == "" || $("#match-5").html() == "") {
        
    }
    else{
        $(".init-page__btn").show();
    }
}

$(".init-page__btn").on("click", function() {

    if ($("#match-1").html() == '<li class="draggable" id="1"><img src="s1.png"></li>') {
        $(".matchable").eq(0).css({
            "background": "#F1F8E9",
            "color":"black"
        })
    } else {
        $(".matchable").eq(0).css({
            "background": "red"
        })
    }
    if ($("#match-2").html() == '<li class="draggable" id="2"><img src="s2.png"></li>') {
        $(".matchable").eq(1).css({
            "background": "#F1F8E9",
            "color":"black"
        })
    } else {
        $(".matchable").eq(1).css({
            "background": "red"
        })
    }
    if ($("#match-3").html() == '<li class="draggable" id="3"><img src="s3.png"></li>') {
        $(".matchable").eq(2).css({
            "background": "#F1F8E9",
            "color":"black"
        })
    } else {
        $(".matchable").eq(2).css({
            "background": "red"
        })
    }
    if ($("#match-4").html() == '<li class="draggable" id="4"><img src="s4.png"></li>') {
        $(".matchable").eq(3).css({
            "background": "#F1F8E9",
            "color":"black"
        })
    } else {
        $(".matchable").eq(3).css({
            "background": "red"
        })
    }
    if ($("#match-5").html() == '<li class="draggable" id="5"><img src="s5.png"></li>') {
        $(".matchable").eq(4).css({
            "background": "#F1F8E9",
            "color":"black"
        })
    } else {
        $(".matchable").eq(4).css({
            "background": "red"
        })
    }
    $(".init-page__btn").hide();
    $(".init-page__btn2").show();
})